package main;

import main.products.Product;
import main.products.ProductList;

public class CompositeProductsTest {

	public static void main(String[] args) {
		ProductList mainProductsList = new ProductList("products list");
		
		ProductList dairyProducts = new ProductList("dairy products");
		ProductList vegetables = new ProductList("vegetables");
		ProductList fruits = new ProductList("fruits");
		
		Product milk = new Product("milk", 10);
		Product cheese = new Product("cheese", 5.6f);
		Product tomato = new Product("tomato", 2.80f);
		Product carrot = new Product("carrot", 1.8f);
		Product apple = new Product("apple", 1f);
		Product chocolate = new Product("chocolate", 4.15f);
		
		dairyProducts.addChild(milk);
		dairyProducts.addChild(cheese);
		
		vegetables.addChild(tomato);
		vegetables.addChild(carrot);
		
		fruits.addChild(apple);
		
		mainProductsList.addChild(dairyProducts);
		mainProductsList.addChild(vegetables);
		mainProductsList.addChild(fruits);
		mainProductsList.addChild(chocolate);
		
		mainProductsList.print();
		System.out.println(System.lineSeparator());
		System.out.println("total price:" + mainProductsList.getPrice());
	}
	
}

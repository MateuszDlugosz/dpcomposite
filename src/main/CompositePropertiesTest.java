package main;

import main.properties.Property;
import main.properties.PropertyList;

public class CompositePropertiesTest {

	public static void main(String[] args) {
		PropertyList baseComponent = new PropertyList("BaseComponent");
		
		PropertyList pList1 = new PropertyList("Component 1");
		PropertyList pList2 = new PropertyList("Component 2");
		PropertyList pList3 = new PropertyList("Component 3");
		
		Property property1 = new Property("name1", "value1");
		Property property2 = new Property("name2", "value2");
		Property property3 = new Property("name3", "value3");
		Property property4 = new Property("name4", "value4");
		Property property5 = new Property("name5", "value5");
		Property property6 = new Property("name6", "value6");
		Property property7 = new Property("name7", "value7");
		Property property8 = new Property("name8", "value8");
		Property property9 = new Property("name9", "value9");
		
		pList1.addChild(property1);
		pList1.addChild(property2);
		pList1.addChild(property3);
		
		pList2.addChild(property4);
		pList2.addChild(property5);
		pList2.addChild(property6);
		
		pList3.addChild(pList1);
		pList3.addChild(property7);
		pList3.addChild(property8);
		pList3.addChild(property9);
		
		baseComponent.addChild(pList2);
		baseComponent.addChild(pList3);
		
		baseComponent.print();
	}

}

package main.products;

/*
 * Leaf
 */
public class Product implements ProductComponent {

	private String name;
	private float price;
	
	public Product(String name, float price) {
		this.name = name;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public float getPrice() {
		return price;
	}

	@Override
	public void print() {
		System.out.println("-" + name + " " + price);
	}

}

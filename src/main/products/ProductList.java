package main.products;

import java.util.ArrayList;
import java.util.List;

/*
 * Composite
 */
public class ProductList implements ProductComponent {

	private String name;
	private List<ProductComponent> products;
	
	public ProductList(String name) {
		this.name = name;
		
		products = new ArrayList<ProductComponent>();
	}

	public String getName() {
		return name;
	}
	
	public void addChild(ProductComponent component) {
		products.add(component);
	}
	
	@Override
	public float getPrice() {
		float allProductsPrice = 0;
		
		for (ProductComponent product : products) {
			allProductsPrice += product.getPrice();
		}
		
		return allProductsPrice;
	}

	@Override
	public void print() {
		System.out.println("+" + name);
		
		for (ProductComponent product : products) {
			product.print();
		}
	}
	
}

package main.properties;

import java.util.ArrayList;
import java.util.List;

/*
 * Composite
 */
public class PropertyList implements PropertiesComponent {

	private String name;
	private List<PropertiesComponent> components;
	
	public PropertyList(String name) {
		this.name = name;
		components = new ArrayList<PropertiesComponent>();
	}
	
	public void addChild(PropertiesComponent component) {
		components.add(component);
	}
	
	@Override
	public void print() {
		System.out.println("component{name='" + name + "', childs=[");
		
		for (PropertiesComponent component : components) {
			component.print();
		}
		
		System.out.println("]}");
	}

}

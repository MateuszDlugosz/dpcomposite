package main.properties;

/*
 * Leaf
 */
public class Property implements PropertiesComponent {

	private String name;
	private String value;
	
	public Property(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
	@Override
	public void print() {
		System.out.println("property[name='" + name + "', value='" + value + "']");
	}

}
